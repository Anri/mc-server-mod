package net.mylloon.mcsm;

import net.fabricmc.api.ModInitializer;

import net.fabricmc.fabric.api.command.v2.CommandRegistrationCallback;
import static net.minecraft.server.command.CommandManager.literal;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class Mcsm implements ModInitializer {
	public static final String MOD_ID = "mcsm";

	// This logger is used to write text to the console and the log file.
	// It is considered best practice to use your mod id as the logger's name.
	// That way, it's clear which mod wrote info, warnings, and errors.
	public static final Logger LOGGER = LoggerFactory.getLogger(MOD_ID);

	@Override
	public void onInitialize() {
		// This code runs as soon as Minecraft is in a mod-load-ready state.
		// However, some things (like resources) may still be uninitialized.
		// Proceed with mild caution.

		// Glow command
		CommandRegistrationCallback.EVENT.register(
				(dispatcher, registryAccess, environnement) -> dispatcher.register(literal("glow").executes(context -> {
					dispatcher.execute("effect give @a minecraft:glowing 10000 1 true", context.getSource());

					return 1;
				})));

		// Clean mobs command
		// TODO: Ignore named entities
		/*
		 * List<String> saved_entities = List.of("minecraft:player",
		 * "minecraft:armor_stand", "#minecraft:arrows",
		 * "minecraft:painting", "minecraft:item_frame", "minecraft:boat",
		 * "minecraft:chest_boat",
		 * "minecraft:villager");
		 * String command = "say kill @e[type=!" + String.join(",type=!",
		 * saved_entities) + "]";
		 *
		 * CommandRegistrationCallback.EVENT.register(
		 * (dispatcher, registryAccess, environnement) -> dispatcher
		 * .register(literal("nettoyage").executes(context -> {
		 * dispatcher.execute(command,
		 * context.getSource());
		 *
		 * return 1;
		 * })));
		 */
	}
}
